package org.lindbergframework.persistence;


import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.lindbergframework.util.ReflectionUtil;


/**
 * Classe que efetua a copia (clone) de uma entidade persistente bem como
 * todo o seu contexto de persistencia incluindo relacionamentos e entidades
 * dependentes e que compoem a mesma.
 * 
 * @author Victor Lindberg (victorlindberg713@gmail.com)
 * 
 */
@SuppressWarnings("rawtypes")
public class PersistenceCloner<E> {
	
	private E entity;
	
	/**
	 * Construtor padrão.
	 */
	public PersistenceCloner(E entity) {
		this.entity = entity;
	}
	
	@SuppressWarnings("unchecked")
	public E generateCopyToPersist() {
        return  (E) doGenerateCopyToPersist(entity, new HashMap<Object, Object>());
    }
	
	/**
     * Gera uma copia da entidade persistente pronta para para a inclusão.
     * 
     * @param entity entitade persistente a ser copiada.
     * @param entityCache cache das entidades que já foram copiados de modo que quando algum bean for passado para ser
     *            copiado antes da copia for feita o cache é verificado para ver se o bean ja foi copiado e
     *            assim essa instancia de copia ser usada ao invés de fazer novamente a copia.
     * @return copia da entidade pronta para para a inclusão.
     */
	protected Object doGenerateCopyToPersist(Object entity,
        Map<Object, Object> entityCache) {
    	Object cacheCopy = entityCache.get(entity);
        if (cacheCopy != null)
            return cacheCopy;

        Object entityCopy = ReflectionUtil.createInstance(entity.getClass());
        entityCache.put(entity, entityCopy);

        Field[] fields = ReflectionUtil.getFields(entity, true, true);

        for (Field entityField : fields) {
            int modifiers = entityField.getModifiers();
            NoPersistenceCopy noPersistenceCopyAnnot = entityField.getAnnotation(NoPersistenceCopy.class);
            if (entityField.isAnnotationPresent(Id.class) || Modifier.isFinal(modifiers) ||
            	(noPersistenceCopyAnnot != null && noPersistenceCopyAnnot.setNull()))
                continue;

            try {
                ReflectionUtil.makeAttributesAccessible(entityField);
                Object fieldValue = entityField.get(entity);
                if (! entityField.isAnnotationPresent(ManyToOne.class) && 
                		isEntity(fieldValue) && 
                		! isNoCopyField(entityField)){
                	Object fieldValueCopy = doGenerateCopyToPersist(fieldValue, entityCache);
                	entityCache.put(fieldValue, fieldValueCopy);
                	Field oneToOneBack = getFieldRelationship(entityCopy, fieldValue, OneToOne.class);
                	if (oneToOneBack != null){
                		ReflectionUtil.setValueByField(oneToOneBack, fieldValueCopy, entityCopy);
                	}
                	fieldValue = fieldValueCopy;
                }else
                   if (fieldValue instanceof Collection) {
                      boolean oneToManyRelationship = entityField.isAnnotationPresent(OneToMany.class);
                      fieldValue = generateCopyCollectionToPersist(entityCopy, (Collection) fieldValue, oneToManyRelationship, entityCache);
                   }
                
                entityField.set(entityCopy, fieldValue);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }

        return entityCopy;
    }
    
    /**
     * Gera uma copia da coleção de entidades pronta para inclusão.
     * 
     * @param entityCopy copia da entidade onde a copia da coleção será setada.
     * @param collection coleção a ser copiada.
     * @param oneToManyRelationship true se o campo corresponde é um atributo de relacionamento 1 x n dentro do mapeamento.
     * @param entityCache cache das entidades que já foram copiados de modo que quando algum bean for passado para ser
     *            copiado antes da copia for feita o cache é verificado para ver se o bean ja foi copiado e
     *            assim essa instancia de copia ser usada ao invés de fazer novamente a copia.
     * @return copia da entidade pronta para para a inclusão.
     * @throws IllegalArgumentException 
     * @throws IllegalAccessException
     */
    @SuppressWarnings("unchecked")
	protected Collection generateCopyCollectionToPersist(Object entityCopy, Collection collection,
                boolean oneToManyRelationship, Map<Object, Object> entityCache)
        throws IllegalArgumentException, IllegalAccessException {
    	
    	Collection collectionCopy;
        if (collection instanceof List) {
            collectionCopy = new ArrayList();
        } else
        	if (collection instanceof Set)
        		collectionCopy = new HashSet();
        	else
        		collectionCopy = (Collection) ReflectionUtil.createInstance(collection.getClass());
        
        if (oneToManyRelationship) {
            for (Object item : collection) {
                if (isEntity(item)) {
                	Object itemCopy = doGenerateCopyToPersist(item, entityCache);
                    Field relationshipBackField = getFieldRelationship(entityCopy, itemCopy, ManyToOne.class);
                    if (relationshipBackField == null)
                    	relationshipBackField = getFieldRelationship(entityCopy, itemCopy, OneToOne.class);
                    
                    if (relationshipBackField != null){
                    	ReflectionUtil.makeAttributesAccessible(relationshipBackField);
                        relationshipBackField.set(itemCopy, entityCopy);
                    }
                    collectionCopy.add(itemCopy);
                }

            }
        } else
            collectionCopy.addAll(collection);

        return collectionCopy;
    }
    
    /**
     * Obtem o field do relacionamento de volta do tipo passado referente a entityCopy dentro de entity
     * @param entityCopy copia da entidade referente ao relacionamento de ida
     * @param entity entidade referente ao relacionamento de volta
     * @param relationshipType tipo do relacionamento:  1 x 1, 1 x n, n x n, etc...
     * @return o campo do relacionamento de volta em entity referente a entityCopy
     */
    protected Field getFieldRelationship(Object entityCopy, Object entity,Class<? extends Annotation> relationshipType) {
        Field[] fields = ReflectionUtil.getFields(entity, true, true);
        for (Field field : fields) {
            if (field.isAnnotationPresent(relationshipType)) {
                if (entityCopy.getClass().equals(field.getType())) {
                    return field;
                }
            }
        }

        return null;
    }
    
    /**
     * Verifica se o bean passado é um bean de entidade.
     * @param bean bean a ser verificado.
     * @return true se o bean corresponde a uma entidade e false caso contrario.
     */
    protected boolean isEntity(Object bean){
    	return bean != null && (bean.getClass().isAnnotationPresent(Entity.class)
    			|| bean.getClass().isAnnotationPresent(Embeddable.class));
    }
    
    protected boolean isNoCopyField(Field field){
    	NoPersistenceCopy noPersistenceCopy = field.getAnnotation(NoPersistenceCopy.class);
    	if (noPersistenceCopy != null){
    		for (Class<?> clazz : noPersistenceCopy.forClasses())
    			if (clazz.isAssignableFrom(entity.getClass()))
    				return true;
    	}
    	
    	return false;
    }
    
	
}
